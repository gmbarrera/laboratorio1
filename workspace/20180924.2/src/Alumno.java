
public class Alumno implements Comparable<Alumno> {
	private String legajo;
	private String nombre;
	private int dni;
	private float cuota;

	public Alumno() {
	}

	public Alumno(String legajo, String nombre, int dni, float cuota) {
		this.legajo = legajo;
		this.nombre = nombre;
		this.dni = dni;
		this.cuota = cuota;
	}

	public String getLegajo() {
		return legajo;
	}

	public void setLegajo(String legajo) {
		this.legajo = legajo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getDni() {
		return dni;
	}

	public void setDni(int dni) {
		this.dni = dni;
	}
	
	public float getCuota() {
		return cuota;
	}

	public void setCuota(float cuota) {
		this.cuota = cuota;
	}

	public String toString() {
		return "Alumno [legajo=" + legajo + ", nombre=" + nombre + ", dni=" + dni + ", cuota=" + cuota  + "]";
	}
	
	public int hashCode() {
		return  this.legajo.hashCode();
	}
	
	/*
	 * Devuelve:
	 * 0 cuando son iguales
	 * negativo cuando el objeto se considera MENOR al recibido por parametro
	 * positivo cuando el objeto se considera MAYOR al recibido por parametro
	 */
	
	public int compareTo(Alumno o) {
		return this.nombre.compareTo(o.getNombre());
	}
	
	
	
	
}
