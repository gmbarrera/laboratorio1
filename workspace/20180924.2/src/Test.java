import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Random;

public class Test {
	
	public static void main(String[] args) {
		
		Random rnd = new Random();
		
		Alumno a1 = new Alumno("001", "Uno", rnd.nextInt(100000), rnd.nextFloat() * 5000 + 2000);
		Alumno a2 = new Alumno("002", "Dos", rnd.nextInt(100000), rnd.nextFloat() * 5000 + 2000);
		Alumno a3 = new Alumno("003", "Tres", rnd.nextInt(100000), rnd.nextFloat() * 5000 + 2000);
		
		
		
		
		
		Alumno[] array = new Alumno[10];		// Esto es un array. Longitud fija al momento de creacion.
		array[0] = a1;
		array[1] = a2;
		array[2] = a3;
		
		ArrayList lista1 = new ArrayList();
		lista1.add(a1);
		lista1.add(666);
		lista1.add(a2);
		lista1.add("No correspondo que este aca");
		lista1.add(a3);
		
		System.out.println(lista1);
		
		System.out.println("-----------------------------");
		
		ArrayList<Alumno> lista2 = new ArrayList<Alumno>();
		lista2.add(a1);
		lista2.add(a2);
		lista2.add(a3);
		
		for(int i = 0; i < lista2.size(); i++) {
			System.out.println(lista2.get(i));
		}
		
		System.out.println("-----------------------------");
		
		for (Alumno x : lista2) {		//foreach - para cada Alumno x que esta en la lista2 hacer
			System.out.println(x);
		}
		
		System.out.println("-----------------------------");
		
		int i_primitivo;
		Integer i_objeto;
		
		ArrayList<Integer> lista3 = new ArrayList<Integer>();
		lista3.add(34);   // Auto boxing, convertir automaticamente 34 en Integer(34)
		lista3.add(new Integer(5));
		lista3.add(12);
		
		for (Integer x : lista3) {
			System.out.println(x);
		}
		
		System.out.println("-----------------------------");
		
		Collections.sort(lista3);
		
		for (Integer x : lista3) {
			System.out.println(x);
		}
		
		System.out.println("-----------------------------");
		
		Integer n1 = new Integer(64);
		Integer n2 = new Integer(64);
		
		System.out.println(n1.compareTo(n2));
		
		
		System.out.println("-----------------------------");
		
		Collections.sort(lista2);
		
		for (Alumno x : lista2) {
			System.out.println(x);
		}
		
		System.out.println("-----------------------------");
		
		Collections.sort(lista2, new ComparadorAlumnoPorDNI());
		
		for (Alumno x : lista2) {
			System.out.println(x);
		}
		
		System.out.println("-----------------------------");
		
		Collections.sort(lista2, new Comparator<Alumno>() {	// Clases anonimas

			public int compare(Alumno o1, Alumno o2) {
				if (o1.getCuota() > o2.getCuota())
					return 1;
				
				if (o1.getCuota() < o2.getCuota())
					return -1;
				
				return 0;
			}
		});
		
		for (Alumno x : lista2) {
			System.out.println(x);
		}
		
		System.out.println("-----------------------------");
		
		
		System.out.println(a1.hashCode());
		System.out.println(a2.hashCode());
		System.out.println(a3.hashCode());
		
		
		System.out.println("-----------------------------");
		
		HashSet<Alumno> conjunto = new HashSet<Alumno>();
		conjunto.add(a1);
		conjunto.add(a2);
		conjunto.add(a3);
		
		for (Alumno a : conjunto) {
			System.out.println(a);
		}
		
		conjunto.remove(a2);
		
		System.out.println("--");
		
		for (Alumno a : conjunto) {
			System.out.println(a);
		}
		
		System.out.println("-----------------------------");
		
		HashMap<String, Alumno> diccionario = new HashMap<String, Alumno>();
		diccionario.put("c001", a1);
		diccionario.put("c002", a2);
		diccionario.put("c003", a3);
		
		for (String a : diccionario.keySet()) {
			System.out.println(a);
		}
		
		System.out.println("--");
		
		for (String clave : diccionario.keySet()) {
			Alumno alumno = diccionario.get(clave);
			System.out.println(clave + "-->" + alumno);
		}
		
		
		
		
	}
}
