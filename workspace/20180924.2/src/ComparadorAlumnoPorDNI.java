import java.util.Comparator;

public class ComparadorAlumnoPorDNI implements Comparator<Alumno> {

	public int compare(Alumno a1, Alumno a2) {
		return a1.getDni() - a2.getDni();
	} 
}


class ComparadorAlumnoPorCuota implements Comparator<Alumno> {

	public int compare(Alumno a1, Alumno a2) {

		if (a1.getCuota() > a2.getCuota())
			return 1;
		
		if (a1.getCuota() < a2.getCuota())
			return -1;
		
		return 0;
	}
	
}