
public class Persona {
	
	// VARIABLES DE ESTADO
	private String name;
	private int age; 
	private long dni;

	// CONSTRUCTORES
	public Persona(long dniNacimiento) {
		dni = dniNacimiento;
	}
	
	public Persona() {
		
	}
	
	public Persona(String name, int age, long dni) {
		this.name = name;
		this.age = age;
		this.dni = dni;
	}

	// GETTER & SETTERS
	public long getDni() {
		return this.dni;
	}
	
	public void setDni(long dni) {
		if (this.dni == 0) {
			this.dni = dni;
		}
	}
	
	public String getName() {
		return this.name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public int getAge() {
		return this.age;
	}
	
	public void setAge(int age) {
		this.age = age;
	}
	
	
	// M�TODOS
	public void saludar() {
		System.out.println("Hola!! Soy " + this.name);
	}
	
	public void saludar(Persona persona) {
		System.out.println("Hola " + persona.name + ", soy "+ this.name);
	}
	
	// Metodos con mismo nombre y distinto parametros: SOBRECARGAR  (OVERLOAD)
	
	
	public String toString() {
		String aux;
		
		aux = "Soy ";
		aux += this.name;
		aux += ", tengo " + this.age;
		
		return aux;
	}
	
	public boolean equals(Object o) {
		Persona unaPersona = (Persona)o;		//  Casting o "Castear"
		
		boolean result = this.getAge() == unaPersona.getAge();
		result = result && this.getDni() == unaPersona.getDni();
		
		//Averiguar como hacer si difieren en mayusculas/minusculas
		result = result && this.getName().equals(unaPersona.getName());
		
		return result;
	}
	
}
