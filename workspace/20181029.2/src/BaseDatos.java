import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

//Build Path -> Configure Build Path -> Libraries -> Add external JAR

public class BaseDatos {
	
	public static void main(String[] args) { 
		Connection conn = null;
		
		try {
			//Abrir la conexion
			Class.forName("org.sqlite.JDBC");
			conn = DriverManager.getConnection("jdbc:sqlite:" + "basedatos.db");
			
			System.out.println("Base conectada.");
			
			File f = new File("basedatos.db");
			if (f.length() == 0) {
				System.out.println("Base vacia, creado base...");
				
				String sql = "CREATE TABLE Alumno (Nombre text, Apellido Text, Legajo int)";
				
				Statement smt = conn.createStatement();
				smt.execute(sql);
				
				System.out.println("Tabla creada");
			}
			
			Statement smt = conn.createStatement();
			smt.execute("INSERT INTO Alumno (Nombre, Apellido, Legajo) VALUES ('Uno', 'One', 11)");
			
			conn.createStatement().execute("INSERT INTO Alumno (Nombre, Apellido, Legajo) VALUES ('Dos', 'Two', 22)");
			
			PreparedStatement prep = conn.prepareStatement("INSERT INTO Alumno (Nombre, Apellido, Legajo) VALUES (?,?,?) ");
			prep.setString(1, "Tres");
			prep.setString(2, "Three");
			prep.setInt(3, 33);
			//prep.setFloat(4, 45.24f);
			prep.execute();
			
			smt = conn.createStatement();
			ResultSet rs = smt.executeQuery("SELECT * FROM Alumno");
			while (rs.next()) {
				System.out.print(rs.getString(1));
				System.out.print("\t" + rs.getString(2));
				System.out.println("\t" + rs.getInt(3));
				
			}
			rs.close();
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (conn != null)
				try {
					conn.close();
				} catch(SQLException e) {
					e.printStackTrace();
				}
		}
	}
}
