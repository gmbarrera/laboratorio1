import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JFrame;

public class Ventana3 extends JFrame {
	public Ventana3() {
		JButton b1 = new JButton("Uno");
		JButton b2 = new JButton("Dos");
		JButton b3 = new JButton("Tres");
		JButton b4 = new JButton("Cuatro");
		JButton b5 = new JButton("Cinco");
		
		//this.setLayout(new GridLayout()); Lo crea con una sola fila, y la cantidad de columnas necesarias
		this.setLayout(new GridLayout(5, 4)); //Prioriza la cantidad de filas vs columnas
											  //Constructor: Filas-Columnas
		this.add(b1);
		this.add(b2);
		this.add(b3);
		this.add(b4);
		this.add(b5);
		
		for(int i = 0; i < 10; i++)
			this.add(new JButton("B" + Integer.toString(i)));
		
		
		
		
	}
}
