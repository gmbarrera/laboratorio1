import java.awt.BorderLayout;

import javax.swing.JButton;
import javax.swing.JFrame;


// Por default los JFrame tienen el BorderLayout seteado

public class Ventana1 extends JFrame {
	
	
	public Ventana1() {
		JButton b1 = new JButton("Centro");
		JButton b2 = new JButton("Sur");
		JButton b3 = new JButton("Norte");
		JButton b4 = new JButton("Este");
		JButton b5 = new JButton("El Lejano Oeste");
		
		//Se agrega el boton al frame
		this.add(b1);
		this.add(b2, BorderLayout.SOUTH);
		this.add(b3, BorderLayout.NORTH);
		this.add(b4, BorderLayout.EAST);
		this.add(b5, BorderLayout.WEST);
		
		
	}
	
	

}
