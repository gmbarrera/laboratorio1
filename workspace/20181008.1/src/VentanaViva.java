import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;

public class VentanaViva extends JFrame {

	public VentanaViva() {
		JButton b1 = new JButton("Push me!! 1");
		JButton b2 = new JButton("Push me!! 2");
		JButton b3 = new JButton("Push me!! 3");
		
		this.setLayout(new GridLayout());
		
		this.add(b1);
		this.add(b2);
		this.add(b3);
		
		
		//Version 1. Con clase anonima para controlar la accion el boton
		b1.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				System.out.println("Me clickearon!!");
			}
		});
		
		
		//Version 2.
		b2.addActionListener(new AccionClickBoton());
		
		
		//Version 3.
		b3.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				accionClick3();
			}
		});
		
	}
	
	public void accionClick3() {
		System.out.println("Click v3");
	}

}

class AccionClickBoton implements ActionListener {

	@Override
	public void actionPerformed(ActionEvent e) {
		System.out.println("Me clickearon!! con clase concreta");
		
	}
	
}
