import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JFrame;

public class Ventana2 extends JFrame {
	
	
	public Ventana2() {
		JButton b1 = new JButton("Uno");
		JButton b2 = new JButton("Dos");
		JButton b3 = new JButton("Tres");
		JButton b4 = new JButton("Cuatro");
		JButton b5 = new JButton("Cinco");


		this.add(b3);
		this.add(b1);
		this.add(b2);
		this.add(b4);
		this.add(b5);
		
		this.setLayout(new FlowLayout());
		// El FlowLayout los ubica en el centro superior del frame
		// en el orden que fueron agregados.
		// El orden respeta de izquierda a derecha y de arriba hacia abajo.
		
	}

}
