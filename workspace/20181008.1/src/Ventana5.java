import java.awt.BorderLayout;
import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class Ventana5 extends JFrame {
	
	public Ventana5() {
		JButton b1 = new JButton("Uno");
		JButton b2 = new JButton("Dos");
		JButton b3 = new JButton("Tres");
		JButton b4 = new JButton("Cuatro");
		JButton b5 = new JButton("Cinco");
		
		JButton b6 = new JButton("-Centro-");
		
		
		JPanel panelSuperior = new JPanel();
		panelSuperior.setLayout(new GridLayout(1, 5));
		
		panelSuperior.add(b1);
		panelSuperior.add(b2);
		panelSuperior.add(b3);
		panelSuperior.add(b4);
		panelSuperior.add(b5);
		
		
		this.add(panelSuperior, BorderLayout.NORTH);
		this.add(b6);
		
		
	}

}
