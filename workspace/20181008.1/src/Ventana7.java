import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextArea; // Mas de una linea de texto
import javax.swing.JTextField;

public class Ventana7 extends JFrame {

	public Ventana7() {
		JTextField t1 = new JTextField();
		JTextArea ta1 = new JTextArea();
		JLabel etiqueta = new JLabel("Soy una etiqueta");

		this.add(t1, BorderLayout.NORTH);
		this.add(ta1);
		this.add(etiqueta, BorderLayout.SOUTH);

		t1.addKeyListener(new KeyListener() {
			
			@Override
			public void keyTyped(KeyEvent arg0) {
				System.out.println("Typed");
			}
			
			@Override
			public void keyReleased(KeyEvent arg0) {
				System.out.println("released");
			}
			
			@Override
			public void keyPressed(KeyEvent arg0) {
				System.out.println("Pressed");
			}
		});
		
		t1.addFocusListener(new FocusListener() {
			
			@Override
			public void focusLost(FocusEvent arg0) {
				System.out.println("Out");
			}
			
			@Override
			public void focusGained(FocusEvent arg0) {
				System.out.println("In");
			}
		});
		
		t1.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				System.out.println("Texto " + t1.getText());
				
			}
		});
	}

}
