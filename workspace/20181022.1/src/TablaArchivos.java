import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Vector;

import javax.swing.DefaultCellEditor;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.event.CellEditorListener;
import javax.swing.event.ChangeEvent;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;

public class TablaArchivos extends JFrame {
	private DefaultTableModel modelo;
	private JTable tabla;
	private JTextField nombre;
	private JTextField apellido;
	private JTextField edad;
	private JButton agregar;

	private JButton grabarArchivo, leerArchivo, limpiarTabla;

	public TablaArchivos() {
		Object[] colnames = { "Nombre", "Apellido", "Edad", "Estado Civil" };

		modelo = new DefaultTableModel(colnames, 2);
		tabla = new JTable(modelo);
		
		nombre = new JTextField();
		apellido = new JTextField();
		edad = new JTextField();

		agregar = new JButton("Agregar");
		grabarArchivo = new JButton("Grabar");
		leerArchivo = new JButton("Leer");
		limpiarTabla = new JButton("Limpiar");

		JPanel panel = new JPanel();
		panel.setLayout(new GridLayout(0, 3));

		panel.add(grabarArchivo);
		panel.add(leerArchivo);
		panel.add(limpiarTabla);

		panel.add(new JLabel("Nombre:"));
		panel.add(nombre);
		panel.add(new JLabel(""));

		panel.add(new JLabel("Apellido:"));
		panel.add(apellido);
		panel.add(new JLabel(""));

		panel.add(new JLabel("Edad:"));
		panel.add(edad);
		panel.add(agregar);

		JComboBox<String> estado = new JComboBox<String>();
		estado.addItem("Casado");
		estado.addItem("Soltero");
		estado.addItem("Separado");
		estado.addItem("Viudo");

		// Recupero la definicion de la columna "Estado Civil"
		TableColumn tc = tabla.getColumnModel().getColumn(modelo.findColumn("Estado Civil"));

		// Se cambia el editor por default (JTextField) de las celda de la columna por
		// el JComboBox
		tc.setCellEditor(new DefaultCellEditor(estado));

		JTextField editorEdad = new JTextField();
		TableColumn tcEdad = tabla.getColumnModel().getColumn(modelo.findColumn("Edad"));
		DefaultCellEditor edadCellEditor = new DefaultCellEditor(editorEdad);
		tcEdad.setCellEditor(edadCellEditor);

		editorEdad.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				System.out.println("Se cambio el texto. Edad.");
			}
		});

		editorEdad.addKeyListener(new KeyListener() {

			@Override
			public void keyTyped(KeyEvent arg0) {
				// TODO Auto-generated method stub

			}

			@Override
			public void keyReleased(KeyEvent arg0) {
				// TODO Auto-generated method stub

			}

			@Override
			public void keyPressed(KeyEvent arg0) {
				if (arg0.getKeyChar() < '0' || arg0.getKeyChar() > '9') {
					// Cancelar la pulsacion

				}

			}
		});

		limpiarTabla.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				modelo.setRowCount(0);
			}
		});
		agregar.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				Object[] datos = new Object[4];

				datos[0] = nombre.getText();
				datos[1] = apellido.getText();
				datos[2] = edad.getText();
				datos[3] = "Soltero";

				modelo.addRow(datos);
			}
		});

		grabarArchivo.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

				BufferedWriter bw = null;

				try {
					bw = new BufferedWriter(new FileWriter(new File("datos.csv")));
					
					//Forma 1
					Vector v = modelo.getDataVector();	
					for (Object o : v) {
						String linea = "";
						for (Object c : (Vector) o) {
							linea += c.toString() + ",";
						}
						bw.write(linea + "\n");
					}
					
					//Forma 2 
					/*
					for(int i = 0; i < modelo.getRowCount(); i++) {
						String linea = "";
						for(int j = 0; i < modelo.getColumnCount(); j++) {
							linea += modelo.getValueAt(i, j) + ",";
						}
						bw.write(linea + "\n");
					}
					*/
					

				} catch (Exception ex) {
					JOptionPane.showMessageDialog(null, "Error: " + ex.getMessage());
				} finally {
					if (bw != null)
						try {
							bw.close();
						} catch (IOException ex) {
							ex.printStackTrace();
						}
				}
			}
		});

		leerArchivo.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				BufferedReader br = null;
				
				modelo.setRowCount(0); //Vacio el modelo
				try {
					//Se abre el archivo
					br = new BufferedReader(new FileReader(new File("datos.csv")));
					String linea;
					while ((linea = br.readLine()) != null) {
						String[] datos = linea.split(",");	//Separo la linea en varios string
						modelo.addRow(datos);	//Se agrega al modelo
					}
				} catch (Exception ex) {
					JOptionPane.showMessageDialog(null, "Error: " + ex.getMessage());
				} finally {
					if (br != null)
						try {
							br.close();
						} catch (IOException ex) {
							ex.printStackTrace();
						}
				}
			}
		});

		this.add(new JScrollPane(tabla));
		this.add(panel, BorderLayout.SOUTH);

	}

	public static void main(String[] arg) {
		TablaArchivos ventana = new TablaArchivos();
		ventana.setTitle("Tabla y Archivos");
		ventana.setSize(400, 250);
		ventana.setVisible(true);
		ventana.setDefaultCloseOperation(EXIT_ON_CLOSE);
	}

}
