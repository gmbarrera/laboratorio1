import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.DefaultCellEditor;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.event.CellEditorListener;
import javax.swing.event.ChangeEvent;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;

public class TableFull extends JFrame {
	private DefaultTableModel modelo;
	private JTable tabla;
	private JTextField nombre;
	private JTextField apellido;
	private JTextField edad;
	private JButton agregar;

	public TableFull() {
		Object[] colnames = { "Nombre", "Apellido", "Edad", "Estado Civil" };

		modelo = new DefaultTableModel(colnames, 2);
		tabla = new JTable(modelo);

		nombre = new JTextField();
		apellido = new JTextField();
		edad = new JTextField();

		agregar = new JButton("Agregar");

		JPanel panel = new JPanel();
		panel.setLayout(new GridLayout(0, 3));

		panel.add(new JLabel("Nombre:"));
		panel.add(nombre);
		panel.add(new JLabel(""));

		panel.add(new JLabel("Apellido:"));
		panel.add(apellido);
		panel.add(new JLabel(""));

		panel.add(new JLabel("Edad:"));
		panel.add(edad);
		panel.add(agregar);

		JComboBox<String> estado = new JComboBox<String>();
		estado.addItem("Casado");
		estado.addItem("Soltero");
		estado.addItem("Separado");
		estado.addItem("Viudo");

		// Recupero la definicion de la columna "Estado Civil"
		TableColumn tc = tabla.getColumnModel().getColumn(modelo.findColumn("Estado Civil"));

		// Se cambia el editor por default (JTextField) de las celda de la columna por
		// el JComboBox
		tc.setCellEditor(new DefaultCellEditor(estado));

		JTextField editorEdad = new JTextField();
		TableColumn tcEdad = tabla.getColumnModel().getColumn(modelo.findColumn("Edad"));
		DefaultCellEditor edadCellEditor = new DefaultCellEditor(editorEdad);
		tcEdad.setCellEditor(edadCellEditor);

		editorEdad.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				System.out.println("Se cambio el texto. Edad.");
			}
		});

		editorEdad.addKeyListener(new KeyListener() {

			@Override
			public void keyTyped(KeyEvent arg0) {
				// TODO Auto-generated method stub

			}

			@Override
			public void keyReleased(KeyEvent arg0) {
				// TODO Auto-generated method stub

			}

			@Override
			public void keyPressed(KeyEvent arg0) {
				if (arg0.getKeyChar() < '0' || arg0.getKeyChar() > '9') {
					// Cancelar la pulsacion

				}

			}
		});

		edadCellEditor.addCellEditorListener(new CellEditorListener() {

			@Override
			public void editingStopped(ChangeEvent arg0) {
				System.out.println("Edicion terminada");
			}

			@Override
			public void editingCanceled(ChangeEvent arg0) {
				System.out.println("Edicion cancelada");
			}
		});

		agregar.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				Object[] datos = new Object[4];

				datos[0] = nombre.getText();
				datos[1] = apellido.getText();
				datos[2] = edad.getText();
				datos[3] = "Soltero";

				modelo.addRow(datos);
			}
		});

		this.add(new JScrollPane(tabla));
		this.add(panel, BorderLayout.SOUTH);

	}

	public static void main(String[] arg) {
		TableFull ventana = new TableFull();
		ventana.setTitle("Ejemplo de tabla");
		ventana.setSize(400, 250);
		ventana.setVisible(true);
		ventana.setDefaultCloseOperation(EXIT_ON_CLOSE);
	}

}
