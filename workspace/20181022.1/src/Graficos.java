import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;

public class Graficos extends JFrame {

	//Esto para cambiar el tama�o de una imagen, solo lo copian y pegan
	private Image getScaledImage(Image srcImg, int w, int h) {
		BufferedImage resizedImg = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);
		Graphics2D g2 = resizedImg.createGraphics();

		g2.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
		g2.drawImage(srcImg, 0, 0, w, h, null);
		g2.dispose();

		return resizedImg;
	}

	public Graficos() {

		ImageIcon ii = new ImageIcon("duck.gif");
		ii = new ImageIcon(this.getScaledImage(ii.getImage(), 70, 70));

		this.add(new JButton(ii));

	}
	
	@Override
	public void paintComponents(Graphics g) {
		super.paintComponents(g);
		
	}

	public static void main(String[] arg) {
		Graficos ventana = new Graficos();
		ventana.setTitle("Ejemplo de Graficos");
		ventana.setSize(400, 250);
		ventana.setVisible(true);
		ventana.setDefaultCloseOperation(EXIT_ON_CLOSE);
	}
}
