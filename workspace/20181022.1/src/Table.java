import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;

public class Table extends JFrame {
	
	private JTable tabla;

    public Table() {
        Object[] colnames = { "Col1", "Col2", "Col3" };
        Object[][] data = { { 1, 2, 3 }, 
                            { "Hola", 5, 6 },
                            { 4, 5.65, 6 }, 
                            { 4, 5, 64.4f } };

        tabla = new JTable(data, colnames);

        this.add(new JScrollPane(tabla));
    }

    public static void main(String[] args) {
    	Table ventana = new Table();
        ventana.setTitle("Ejemplo de tabla");
        ventana.setSize(400, 250);
        ventana.setVisible(true);
        ventana.setDefaultCloseOperation(EXIT_ON_CLOSE);
    }
	
	
}
