import java.awt.BorderLayout;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;


public class ConScroll extends JFrame {
	
	private JTextField unaLinea;
	private JTextArea muchasLineas;
	private JScrollPane scroll;
	
	public ConScroll() {
		
		unaLinea = new JTextField();
		muchasLineas = new JTextArea();
		scroll = new JScrollPane(muchasLineas);
		
		this.add(unaLinea, BorderLayout.NORTH);
		this.add(scroll);
	}
	
	
	public static void main(String[] args) {
		ConScroll cs = new ConScroll();
		cs.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		cs.setSize(400, 400);
		cs.setVisible(true);
		
		
	}
	
	
}
