import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.Buffer;

public class Archivos {

	public static void main(String[] args) {
		BufferedWriter bw = null, bw2 = null;
		BufferedReader br = null;
		
		try {
			bw2 = new BufferedWriter(new FileWriter(new File("dos.txt")));
			bw2.write("El otro archivo");
			
			
			File f = new File("hola.txt");
			FileWriter fw = new FileWriter(f);
			bw = new BufferedWriter(fw);
			
			bw.write("Este es mi primer archivo\n");
			bw.write("Segunda linea");
			
			int x = 1 / 0;
			
			FileReader fr = new FileReader(f);
			br = new BufferedReader(fr);
			
			String linea;
			while ((linea = br.readLine()) != null) {
				System.out.println(linea);
			}
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception ex) {
			System.err.println("Error");
		} finally {

			if (bw != null)
				try {
					bw.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			
			if (bw2 != null)
				try {
					bw2.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			
			if (br != null)
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
		}
		
		
		
		System.out.println("Termine");
		
		
	}
	
}
