
public class Persona {
	
	//En las variables de estado, estas variables SE AUTOINICIALIZAN
	//Todas las numericas se ponen en CERO
	
	public String name;
	public int age; 
	
	private long dni;
	
	public long getDni() {		// getter
		return dni;
	}
	
	public void setDni(long nuevoDni) {
		if (dni == 0) {
			dni = nuevoDni;
		}
	}
	
	
	// M�todo Constructor, se ejecuta cuando creo un nuevo objeto
	public Persona(long dniNacimiento) {
		dni = dniNacimiento;
	}
	
	public Persona() {	// Genero el constructor por default 
		
	}
	
	
	
}


/*
public
private

protected
"package"		<--- default

*/