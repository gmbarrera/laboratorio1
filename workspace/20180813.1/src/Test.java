
public class Test {

	public static void main(String[] args) {
		
		Persona juan = new Persona(40123123);	//Se crea un objeto del tipo persona, y se "referencia" con la variable "juan"
												//  new Persona() Devuelve una referencia al objeto creado, y es asignado a la variable de referencia juan
		juan.name = "Juan";
		juan.age = 20;
		// juan.dni = 40123123;
		
		System.out.println(juan.getDni());
		
		long elDniDeJuan = juan.getDni();
		
		System.out.println(elDniDeJuan);
		
		
		Persona pepe = new Persona();
		System.out.println("pepe: " + pepe.getDni());
		
		pepe.setDni(56123123);
		System.out.println("pepe: " + pepe.getDni());
		
		
		juan.setDni(57123123);
		System.out.println("Juan: " + juan.getDni());
		
		
		
		
		/*
		long unNumero;
		System.out.println(unNumero);		No funciona, pq no est� inicializada la variable unNumero, puesto que es una variable local
		
		*/
		
		
		
		
		
		//juan.dni = 45678;		// No deberia poder hacerlo
		
		//System.out.println(juan.dni);
		
		
		
		/*
		Persona pedro;
		
		pedro = juan;
		
		System.out.println(juan.name);
		System.out.println(pedro.name);
		*/
		
		
		

	}

}
