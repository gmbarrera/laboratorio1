
public class Player {
	// nombre -> String o alfanumerico
	// genero -> ??? (Hombre, Mujer, Otro/a)
	// nickname -> String
	// edad -> entero, positivo
	// email -> String, pero OJO, no puede tener cualquier forma: xxxx@yyyy.zz
	// direccion -> Address

	String name;
	String nickname;
	int age; // Tipo de dato primitivo (float, double, char, long, boolean, short, byte)
	String email;
	Address deliveryAddress, realAddress; // Notacion camelCase

}
