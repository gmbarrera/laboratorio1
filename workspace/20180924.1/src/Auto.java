
public class Auto<T extends Motor> {	//La clase auto se especializa con la clase T

	private T motor;
	

	public Auto() {
		
	}
	
	public Auto(T motor) {
		this.motor = motor;
	}
	
	public T getMotor() {
		return motor;
	}

	public void setMotor(T motor) {
		this.motor = motor;
	}





	public static void main(String[] args) {
		Auto<MotorV8> a = new Auto<MotorV8>(new MotorV8());
		
		System.out.println(a.getMotor());
		
		
		//Auto<Integer> b = new Auto<Integer>(new Integer(56));
		//System.out.println(b.getMotor());
		
	}
}