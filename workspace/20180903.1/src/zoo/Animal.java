package zoo;

public abstract class Animal {
	private String nombre;
	private float peso;
	private int fechaNacimiento;
	private Genero genero;

	public Animal() {
		
	}
	
	public Animal(String nombre) {
		this.nombre = nombre;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public float getPeso() {
		return peso;
	}

	public void setPeso(float peso) {
		this.peso = peso;
	}

	public int getFechaNacimiento() {
		return fechaNacimiento;
	}

	public void setFechaNacimiento(int fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}

	public Genero getGenero() {
		return genero;
	}

	public void setGenero(Genero genero) {
		this.genero = genero;
	}
	
	
	public void comer(String comida) {
		System.out.println("Comiendo feliz: " + comida);
	}
	
	
}
