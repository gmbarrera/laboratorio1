package zoo;

public class Cuidador {
	private String nombre;
	private Animal a1, a2, a3;

	public Cuidador() {
		
	}
	
	public Cuidador(String nombre) {
		this.nombre = nombre;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	public void cuidar(Animal a) {	
		if (a1 == null) {
			a1 = a;
		}
		else if (a2 == null) {
			a2 = a;
		}
		else if (a3 == null) {
			a3 = a;
		}
		else {
			System.out.println("Estoy hasta las manos....");
		}
	}	
	
	public void ordenarComer() {
		if (a1 != null) {
			if (a1 instanceof Herviboro) {		//NO HACER IF ASI DE FEOS!!!!
				a1.comer("Lechuga");
			} else {
				a1.comer("Carne");
			}
			
		}
		if (a2 != null) {
			if (a2 instanceof Herviboro) {
				a2.comer("Lechuga");
			} else {
				a2.comer("Carne");
			}
		}
		if (a3 != null) {
			if (a3 instanceof Herviboro) {
				a3.comer("Lechuga");
			} else {
				a3.comer("Carne");
			}
		}
	}
	
	public void pasear() {
		if (a1 != null) {
			if (a1 instanceof Caminador) {
				((Caminador) a1).caminar();
			}
		}
			
	}
	
}
