package zoo;

public class Test {

	public static void main(String[] args) {
		Cuidador c = new Cuidador();

		Animal an = new Delfin();
		Herviboro el = new Elefante();
		Tigre tr = new Tigre();
		
		c.cuidar(el);
		c.ordenarComer();
		
		System.out.println();
		
		c.cuidar(tr);
		c.ordenarComer();
		
		System.out.println();
		
		c.pasear();
		
	}

}
