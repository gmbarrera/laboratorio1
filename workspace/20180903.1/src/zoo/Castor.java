package zoo;

public class Castor extends Herviboro implements Nadador, Caminador {

	public void caminar() {
		System.out.println("Castor caminando...");
	}

	public void nadar() {
		System.out.println("Castor nadando...");
	}

}
