package zoo;

public class Zoo {
	private String nombre;
	private Cuidador[] cuidadores;
	private Animal[] animales;
	private static Zoo miZoo;
	
	private Zoo() {
		
	}
	
	// Patron de dise�o: Singleton
	
	public static Zoo getInstance() {
		
		if (miZoo == null)
			miZoo = new Zoo();
		
		return miZoo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	
}
