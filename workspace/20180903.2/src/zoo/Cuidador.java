package zoo;

public class Cuidador {
	private String nombre;
	private Animal[] animales;
	private int contador;

	// Metodo constructor
	public Cuidador(int cantidadAnimales) {
		this.animales = new Animal[cantidadAnimales];
	}

	// Metodo constructor
	public Cuidador(int cantidadAnimales, String nombre) {
		this(cantidadAnimales); // Invoca al constructor

		this.nombre = nombre;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public void cuidar(Animal a) {
		if (contador < animales.length) {
			this.animales[contador++] = a; // contador++ postincremental, primero USA el contenido de la variable y
											// luego lo suma 1
		} else {
			System.out.println("No doy mas.... :(");
		}

	}

	public void ordenarComer() {
		for (int i = 0; i < this.animales.length; i++) {
			if (this.animales[i] != null) {
				if (this.animales[i] instanceof Herviboro) { // NO HACER IF ASI DE FEOS!!!!
					this.animales[i].comer("Lechuga");
				} else {
					this.animales[i].comer("Carne");
				}
			}
		}
	}


	public void pasear() {
		for (int i = 0; i < this.animales.length; i++) {
			if (this.animales[i] != null) {
				if (this.animales[i] instanceof Caminador) {
					((Caminador) this.animales[i]).caminar();
				} else {
					((Nadador) this.animales[i]).nadar();
				}
			}
		}
	}

}
