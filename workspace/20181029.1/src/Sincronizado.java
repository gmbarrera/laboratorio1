

class DeadLock {
	private String nombre;
	
	public DeadLock(String nombre) {
		this.nombre = nombre;
	}
	
	public synchronized void saludar(DeadLock dl) {
		System.out.println("Hola " + dl.nombre);
		
		try {
			Thread.sleep(500);
		} catch(InterruptedException e) {
			e.printStackTrace();
		}
		
		dl.devolverSaludo(this);
	}
	// synchronized: entra un proceso y hasta que no salga, no puede entrar
	// otro, queda en espera.
	public synchronized void devolverSaludo(DeadLock dl) {
		System.out.println("Como va " + dl.nombre);
	}
}

public class Sincronizado {

	public static void main(String[] args) {
		DeadLock a1 = new DeadLock("Uno");
		DeadLock a2 = new DeadLock("Dos");
		
		System.out.println("------------------------------------");
		
			//Runnable es una interface
		new Thread(new Runnable() { 
			public void run() {
				a1.saludar(a2);
			}
		}).start();
		
		
		System.out.println("------------------------------------");
		new Thread(new Runnable() { 
			public void run() {
				a2.saludar(a1);
			}
		}).start();
		
		System.out.println("------------FIN---------------------");
		

	}

}
