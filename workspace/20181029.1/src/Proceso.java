class Hilo1 extends Thread {
	private String nombre;
	private int mili;
	
	public Hilo1(String nombre, int mili) {
		this.nombre = nombre;
		this.mili = mili;
	}
	
	public void run() {
		for (int i = 0; i < 20; i++) {
			try {
				this.sleep(mili);
				System.out.println(nombre + " " + i);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
}


public class Proceso {

	public static void main(String[] args) throws InterruptedException {
		
		System.out.println("Comienza proceso...");
		
		Hilo1 h1 = new Hilo1("Soy El Uno", 500);
		h1.start();
		
//		Hilo1 h2 = new Hilo1("Soy El Dos", 1000);
//		h2.start();
	
		
//		h1.join();  // Espera que h1 termine su ejecución
		
		int i = 0;
		while (h1.isAlive()) {
			System.out.println("Procesando...");
		}
		
		

		System.out.println("Termino.");
	}

}


/*
29-Oct-2018	Threads.Base de datos.
5-Nov-2018	Entrega TP / Repaso
12-Nov-2018	2do. Parcial
19-Nov-2018	Feriado
26-Nov-2018	Recuperatorio
3-Dec-2018	Recuperatorio Fuera de cursada

*/
