
public class Test1 {

	public static void main(String[] args) {

		Creador c = new Creador();

		Gato g1 = c.crearGato();
		Gato g2 = c.crearGato();

		// El creador desaparece y aparece otro

		Creador c2 = new Creador();
		Gato g3 = c2.crearGato();

		System.out.println("El ultimo gato creado es el " + c2.getIdGato());
		System.out.println("El ultimo gato creado es el " + Creador.getIdGato());
		
	}
}