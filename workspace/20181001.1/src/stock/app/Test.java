package stock.app;

import stock.exceptions.ExcedeLimiteException;
import stock.modelo.Articulo;
import stock.modelo.Movimiento;
import stock.modelo.Ubicacion;

public class Test {

	public static void main(String[] args) {
		Articulo a = new Articulo(1, "uno");
		Ubicacion u1 = new Ubicacion("Estante 1", 10);
		Ubicacion u2 = new Ubicacion("Estante 2", 15);
		
		//Poner 3 piezas del articulo 'uno' en el 'estante 1'
		
		try {
			
			a.ubicar(u1, 30);
			//int x = 1 / 0;
			//throw new Exception("ahhhh");
		
		} catch (ExcedeLimiteException ele) {
			System.out.println("Se excede el limite");
			System.out.println(ele.getMessage());
			
		} catch (NullPointerException npe) {
			System.out.println("Es NULL :( ");
		} catch (Exception e) {
			System.out.println("No se que paso");
			System.out.println(e.getMessage());
			e.printStackTrace();
		}	
		
		System.out.println("ooooo");
		
		
		/*
		//Poner 4 piezas el articulo 'uno' en el 'estante 2'
		a.ubicar(u2, 4);
		
		System.out.println(a.getStock());
		
		a.registrarMovimiento(new Movimiento("20181001", null, u2, "compra", 2));
		
		System.out.println(a.getStock());
		*/
	}

}
