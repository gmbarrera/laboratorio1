package stock.exceptions;

public class ExcedeLimiteException extends Exception {

	public ExcedeLimiteException(String msg) {
		super(msg);  //Se lo paso al constructor de Exception
	}
}
