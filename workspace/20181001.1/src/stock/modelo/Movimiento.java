package stock.modelo;

public class Movimiento {
	private String fecha;     //yyyymmdd
	private Ubicacion origen;
	private Ubicacion destino;
	private String detalle;
	private int cantidad;
	
	public Movimiento(String fecha, Ubicacion origen, Ubicacion destino, String detalle, int cantidad) {
		this.fecha = fecha;
		this.origen = origen;
		this.destino = destino;
		this.detalle = detalle;
		this.cantidad = cantidad;
	}

	public String getFecha() {
		return fecha;
	}

	public Ubicacion getOrigen() {
		return origen;
	}

	public Ubicacion getDestino() {
		return destino;
	}
	
	public String getDetalle() {
		return detalle;
	}
	
	public int getCantidad() {
		return cantidad;
	}

	@Override
	public String toString() {
		return "Movimiento [fecha=" + fecha + ", origen=" + origen + ", destino=" + destino + ", cantidad=" + cantidad
				+ "]";
	}
}
