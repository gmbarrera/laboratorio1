package stock.modelo;

public class Ubicacion {
	private String lugar;
	private int capacidad;
	
	public Ubicacion(String lugar, int capacidad) {
		this.lugar = lugar;
		this.capacidad = capacidad;
	}

	public String getLugar() {
		return lugar;
	}

	public void setLugar(String lugar) {
		this.lugar = lugar;
	}

	public int getCapacidad() {
		return capacidad;
	}

	public void setCapacidad(int capacidad) {
		this.capacidad = capacidad;
	}

	@Override
	public String toString() {
		return "Ubicacion [lugar=" + lugar + ", capacidad=" + capacidad + "]";
	}
	
	
	
	
}
