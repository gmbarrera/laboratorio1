package stock.modelo;

import java.util.ArrayList;
import java.util.HashMap;

import stock.exceptions.ExcedeLimiteException;

public class Articulo {
	private int id;
	private String descripcion;
	private HashMap<Ubicacion, Integer> ubicaciones;
	private ArrayList<Movimiento> movimientos;
	
	public Articulo() {
		this.ubicaciones = new HashMap<>();
		this.movimientos = new ArrayList<>();
	}
	
	public Articulo(int id, String descripcion) {
		this(); //Invocacion al contructor
		
		this.id = id;
		this.descripcion = descripcion;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public HashMap<Ubicacion, Integer> getUbicaciones() {
		return ubicaciones;
	}
	
	public ArrayList<Movimiento> getMovimientos() {
		return this.movimientos;
	}
	
	
	public int getStock() {
		int stock = 0;
		
		for(Ubicacion u : this.ubicaciones.keySet()) {
			stock += this.ubicaciones.get(u);	// Del HashMap devuelve el valor asociado a la clave 'u'
		}
		
		return stock;
	}
	
	public void ubicar(Ubicacion ubic, int cantidad) throws ExcedeLimiteException {
		if (cantidad > ubic.getCapacidad()) {
			// No se puede ubicar
			//System.err.println("No hay lugar.");
			
			// Arrojar la Exception para que el invocador decida que hacer
			throw new ExcedeLimiteException("La capacidad permitida es: " + ubic.getCapacidad());
			
		} else {
			int cantidadAUbicar = cantidad;
			
			if (this.ubicaciones.containsKey(ubic))
				cantidadAUbicar += this.ubicaciones.get(ubic);
				
			this.ubicaciones.put(ubic, cantidadAUbicar);
		}
	}
	
	public void registrarMovimiento(Movimiento mov) throws ExcedeLimiteException {
		if (mov.getCantidad() > 0) {
			// Agregando
			ubicar(mov.getDestino(), mov.getCantidad());
			this.movimientos.add(mov);
			
		} else {
			// Sacando
			
			
		}
		
	}
	
	
}
