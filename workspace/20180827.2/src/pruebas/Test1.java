package pruebas;

import figuras.Circulo;
import figuras.Figura;
import figuras.Figura2D;
import figuras.Triangulo;

public class Test1 {

	public static void main(String[] args) {

		// Figura f = new Figura();  da error, pq Figura es Abstracta
		
		Circulo c1 = new Circulo(1);
		System.out.println(c1.calcularPerimetro());
		
		Figura f1 = c1;
		Figura2D x = c1;
		
		System.out.println(x.calcularArea());
		
		
		Triangulo t1 = new Triangulo(10, 20);
		System.out.println(t1.calcularArea());
		
		Figura2D y = t1;
		System.out.println(y.calcularArea());
		
		y = c1;
		System.out.println(y.calcularArea());
		
		// POLIMORFISMO
		
	}

}
