package figuras;

public class Triangulo extends Figura2D {
	
	private double base, altura;
	
	public Triangulo(double base, double altura) {
		super("Triangulo");
		
		this.base = base;
		this.altura = altura;
	}

	public double getAltura() {
		return altura;
	}

	public void setAltura(double altura) {
		this.altura = altura;
	}

	public double getBase() {
		return base;
	}

	public void setBase(double base) {
		this.base = base;
	}

	@Override
	public double calcularPerimetro() {
		
		return 0;
	}
	
	public double calcularArea() {
		return base * altura / 2;
	}
	
	

}
