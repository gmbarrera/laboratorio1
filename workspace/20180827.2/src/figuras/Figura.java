package figuras;

public abstract class Figura {
	
	private String name;

	public Figura(String name) {
		this.name = name;
		System.out.println("Constructor de Figura");
	}
	
	public String getName() {
		return name;
	}

	
}
