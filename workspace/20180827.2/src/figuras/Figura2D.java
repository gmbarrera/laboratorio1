package figuras;

public abstract class Figura2D extends Figura {
	
	public abstract double calcularPerimetro();
	public abstract double calcularArea();
	
	public Figura2D(String name) {
		super(name);	//Invocar al constructor de la superclase

		System.out.println("Constructor de Figura2D");
		
	}
	
}
