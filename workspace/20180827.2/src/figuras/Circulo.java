package figuras;

public class Circulo extends Figura2D {
	
	private float radio;
	
	public Circulo(float radio) {
		super("Circulo");
		
		System.out.println("Constructor de Circulo");
		
		this.radio = radio;
	}
	
	public float getRadio() {
		return radio;
	}

	public void setRadio(float radio) {
		if (radio > 0) {
			this.radio = radio;
		}
	}

	public double calcularPerimetro() {
		
		return 2 * Math.PI * this.radio;
	}
	
	public double calcularArea() {
		
		return Math.PI * this.radio * this.radio;
	}

}
