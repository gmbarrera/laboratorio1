
public class Calculadora {
	
	// Patron de dise�o: Strategy
	
	private IModoCalculadora modoActual = new ModoDecimal();

	
	public void cambiarModo(IModoCalculadora modo) {
		this.modoActual = modo;
	}
	
	
	public void sumar(int i, int j) {
		String res = modoActual.sumar(i, j);
		
		System.out.println(res);
	}
	
	
	
	
}
