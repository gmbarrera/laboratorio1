package socket;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class Client {

	private Socket clientSocket;
	private PrintWriter out;
	private BufferedReader in;

	public void startConnection(String ip, int port) {
		try {
			clientSocket = new Socket(ip, port);
			out = new PrintWriter(clientSocket.getOutputStream(), true);
			in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
		} catch (IOException e) {
			System.err.println(e);
		}
	}

	public String sendMessage(String msg) {
		try {
			out.println(msg);
			return in.readLine();
		} catch (Exception e) {
			return null;
		}
	}

	public void stopConnection() {
		try {
			in.close();
			out.close();
			clientSocket.close();
		} catch (IOException e) {
			System.err.println(e);
		}

	}
	
	public static void main(String[] args) {
		Client client1 = new Client();
	    client1.startConnection("127.0.0.1", 9695);
	    String msg1 = client1.sendMessage("hello");
	    String msg2 = client1.sendMessage("world");
	    String terminate = client1.sendMessage("end");
	    
	    System.out.println(msg1);
	    System.out.println(msg2);
	    System.out.println(terminate);
	    
	    
	}
	
}
