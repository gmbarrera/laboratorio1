package socket;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

public class Server {

	private ServerSocket serverSocket;
	private ArrayList<Socket> clients;
	
	

	public void start(int port) throws IOException {
		this.clients = new ArrayList<>();
		this.serverSocket = new ServerSocket(port);
		
		while (true)
			new ClientHandler(serverSocket.accept()).start();
	}

	public void stop() throws IOException {
		serverSocket.close();
	}

	private class ClientHandler extends Thread {
		private Socket clientSocket;
		private PrintWriter out;
		private BufferedReader in;

		public ClientHandler(Socket socket) {
			this.clientSocket = socket;
		}

		public void run() {
			try {
				out = new PrintWriter(clientSocket.getOutputStream(), true);
				in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));

				String inputLine;
				while ((inputLine = in.readLine()) != null) {
					if ("end".equals(inputLine)) {
						out.println("bye");
						break;
					}
					out.println(inputLine);
				}

				in.close();
				out.close();
				clientSocket.close();
			} catch (IOException e) {
				System.err.println(e);
			}
		}
	}

	public static void main(String[] args) throws IOException {
		Server s = new Server();
		s.start(9695);
	}
}
