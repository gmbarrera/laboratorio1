import java.util.Random;
import java.util.Scanner;

public class Varios {

	public static void main(String[] args) {

		Random rnd = new Random();
		
		//Genera un numero entero
		System.out.println(rnd.nextInt());
		
		//Genera un numero entero entre 0 y 99
		System.out.println(rnd.nextInt(100));
		
		
		//Leer desde teclado
		String nombre;
		Scanner lector = new Scanner(System.in);
		
		System.out.print("Ingrese su nombre: ");
		nombre = lector.next();
		
		System.out.println("Hola " + nombre);
		
		
		//Convertir String a int
		String numero = "12";
		int n = Integer.parseInt(numero);
		
	}

}
