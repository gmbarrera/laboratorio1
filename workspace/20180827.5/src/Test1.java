
enum Calles {
	MARIO_BRAVO("Mario Bravo"),
	CABRERA("Cabrera"),
	GORRITI("Gorriti"),
	CORDOBA("Av.Cordoba");
	
	private String nombre;
	
	private Calles(String nombre) {
		this.nombre = nombre;
	}
	
	public String toString() {
		return this.nombre;
	}
	
}


public class Test1 {

	public static void main(String[] args) {
		
		Calles c1 = Calles.MARIO_BRAVO;
		
		if (c1 == Calles.MARIO_BRAVO) {
			System.out.println("ES");
		}
		
		System.out.println("Usted esta en " + c1);
		
		
		

	}

}
