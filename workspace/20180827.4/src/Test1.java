
public final class Test1 {
	
	public final int soyConstante;
	
	public Test1() {
		this.soyConstante = 5;
	}
	
	public final void noMeCambies() {
		System.out.println("No me cambies");
	}
	
	
	public static void main(String[] args) {
		
		Test1 t1 = new Test1();
		
		//t1.soyConstante = 11;
		
		
		

	}

}


/*
class Test2 extends Test1 {			no se puede pq Test1 es final y no se puede heredar
	
	public void noMeCambies() {				No se puede pq el metodo es final
		System.out.println("Te cambie");
	}
	
}

*/